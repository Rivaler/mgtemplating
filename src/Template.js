import TemplateInstance from './TemplateInstance';

export default class Template
{
    constructor(templateId, defaults = {}, events = {})
    {
        const tplHTML = Template.getTemplateHTML(templateId);
		this.defaults = defaults;
		this.events = events;
		this.tplHTML = tplHTML;
    }

    static makeNode = function(templateId, data){
		return new Template(templateId, data).makeNode();
	}

    static getTemplateHTML(templateId)
    {
        // TODO - Verificare che ci sia solo un elemento figlio
        const tpl = document.getElementById(templateId);
        if(!tpl)
            throw "Template not found: " + templateId;

        return tpl.innerHTML;
    }

	makeInstance(){
		return new TemplateInstance(this);
	}

	makeNode(data) {
		if(!data)
			data = {};

		return this.makeInstance().set(data).toNode();
	}
}

