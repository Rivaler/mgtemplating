export default class TemplateInstance
{
    constructor(template)
    {
        this.template = template;
		this.data = {};
		this.events = {};
		this.node = false;
    }

    static replaceTags(html, data)
    {
        for(const placeholder in data)
        {
            if(data.hasOwnProperty(placeholder))
            {
                const value = data[placeholder];
                const pattern = new RegExp('{{' + placeholder + '}}', 'g');

                html = html.replace(pattern, value);
            }
        }

        return html;
    }

    static addEventListeners(node, events)
    {
        for(const event in events)
        {
            if(events.hasOwnProperty(event))
            {
                const action = events[event];

                node.addEventListener(event, action);
            }
        }

        return node;
    }

    toNode() {

		if(!this.node)
		{
			const data = Object.assign({}, this.template.defaults, this.data);
			const events = Object.assign({}, this.template.events, this.events);
			const parsedHTML = TemplateInstance.replaceTags(this.template.tplHTML, data);

			const node =  document.createRange().createContextualFragment(parsedHTML).firstElementChild;
			const nodeWithEvents = TemplateInstance.addEventListeners(node, events);

			this.node = nodeWithEvents;
		}

		return this.node;

	}

	set(tags, value) {

		if(typeof tags === 'string')
		{
			const obj = {};
			obj[tags] = value;
			tags = obj;
		}

		this.data = Object.assign({}, this.data, tags);

		this.invalidateNode();

		return this;

	}

	setEventListener(events, action) {
		if(typeof events === 'string')
		{
			const obj = {};
			obj[events] = action;
			events = obj;
		}

		this.events = Object.assign({}, this.events, events);

		this.invalidateNode();

		return this;
	}

	clear(){
		this.data = {};
	}

	invalidateNode(){
		this.node = false;
	}
}