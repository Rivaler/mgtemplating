const path = require('path');

module.exports = {
    entry: {
        "mg-templating.min" : './src/index.js'
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'js'),
    },
    mode : "production"
};

